// TITLE: Tuple vs Array
import UIKit

// =========================================================================================================
/*
Homework Assignment #2: Collections
What are the differences between a tuple and an array? When would each be appropriate too use? In your own words, write 1 - 2 paragraphs explaining the different use-cases for each.
*/

/*
To earn extra credit, add an explanation of "ranges" to the top of your document. What are ranges? And why would you use one instead of an array or tuple? Add a code example for ranges as well.
*/
// =========================================================================================================


// Difference between Tuples and arrays:
/*
    A tuple is used to pass around a few values that typically need to be stored together. For example, if you need to store an item name, that item's cost, and that item's quanity then a tuple would be more suitable. This will keep all the data together.
    An array is used to list values that do not necessarily have to be grouped together or depend on each other and they are typically of the same type. If there are many values or creating a list an array would be more suitable.
*/

// When would each be appropriate too use?
/*
    When to use a Tuple : When multiple values depend on each other.
    When to use an Array: When the collection is a list of elments or there are many values that need to be stored in a collection.
*/


// Use-cases for Tuple (1-2 Paragraphs):
/*    If you are creating a varible called employee which has a few pieces of values you would like to keep together you can use tuples. By doing this the variable employee can store all the information. Tuples are typically ralated to each other and not just a random list of values. Tuples are also used to return multiple values together.

*/

// Use-cases for an Array (1-2 Paragraphs):
/*
    If you need to store a list of numbers that have no real connection other than being the same type. You also have a list of string; for instance, having a variable fruits of type array can list all the names of fruits. An array keeps a list of data in a collection but it is not necessary for these values to depend on each other. You can also have an array of tuples which would be a list of tuples and each tuple does not depend on each other; however, inside the tuple you can keep the data you want to keep together stored in one element.
*/

// Explanation of "ranges" - What are ranges and why is it used instead of an array or tuple?:
/*
    Ranges are a interval of values. For example 1 through 5 is an interval. Using ranges instead of an array or tuple are typically for putting bounds on values. For instance, only wanting the first 3 elements of an array or wanting to go through a for loop 5 times.
*/

// Example code of ranges:

for numbers in 1...5 {
    print(numbers)
}

let rangeNumber = ..<10;
print(rangeNumber.contains(8));  // true
print(rangeNumber.contains(13)); // false

// Then, within the document write 2 code examples (1 for arrays and 1 for tuples) showcasing the use-cases you explained above.

// Tuples:
var employee = (name: "Mike", badgeNumber: 5595, role: "Clerk", wage: 15.75);

let name : String = employee.name;
let badge: Int = employee.badgeNumber;
let role : String = employee.role;
let wage : Double = employee.wage;


// Array:
var numbers : [Int] = [23,2,345,77];

// adding new numbers to the array
numbers.append(99);
numbers.append(5);

// remove the first element for the array:
numbers.removeFirst();

// list from range 0...2 (23 is removed so array should start with 2)
let numbers2 = numbers[0...2];


